use std::collections::HashMap;
use std::sync::{Arc, Mutex, RwLock};

use camino::{Utf8Path, Utf8PathBuf};
use codespan_reporting::files::Files;
use codespan_reporting::term::termcolor::Buffer;
use color_eyre::eyre::{anyhow, bail, Context};
use spade::{stdlib_and_prelude, Artefacts, ModuleNamespace, UnfinishedArtefacts};
use spade_common::location_info::{Loc, WithLocation};
use spade_common::name::Identifier;
use spade_diagnostics::diagnostic::DiagnosticLevel as SpadeDiagnosticLevel;
use spade_diagnostics::Diagnostic as SpadeDiagnostic;
use spade_diagnostics::{CodeBundle, DiagHandler, Emitter};
use spade_hir::ItemList;
use swim::libraries::{LockFile, RestoreAction};
use swim::spade::{Namespace, SpadeFile};
use swim::{libs_dir, lock_file, src_dir};
use tokio::runtime::Handle;
use tower_lsp::jsonrpc::Result;
use tower_lsp::lsp_types::{
    Diagnostic, DiagnosticSeverity, DidChangeTextDocumentParams, DidOpenTextDocumentParams,
    DidSaveTextDocumentParams, InitializeParams, InitializeResult, InitializedParams, Location,
    MessageType, OneOf, Position, Range, ServerCapabilities, SymbolInformation, SymbolKind,
    TextDocumentSyncCapability, TextDocumentSyncKind, Url, WorkspaceSymbolParams,
};
use tower_lsp::{Client, LanguageServer, LspService, Server};

fn loc_to_location(loc: Loc<()>, code: &CodeBundle) -> color_eyre::Result<Location> {
    let Loc {
        inner: _,
        span,
        file_id,
    } = loc;

    let file = code
        .files
        .get(file_id)
        .with_context(|| format!("get file corresponding to file_id {file_id}"))?;
    let path = Utf8PathBuf::from(file.name().to_string());
    let codespan_reporting::files::Location {
        line_number: start_line,
        column_number: start_column,
    } = file
        .location((), span.start().to_usize())
        .with_context(|| {
            format!(
                "line and column of byte offset {} in {}",
                span.start(),
                path
            )
        })?;
    let codespan_reporting::files::Location {
        line_number: end_line,
        column_number: end_column,
    } = file
        .location((), span.end().to_usize())
        .with_context(|| format!("line and column of byte offset {} in {}", span.end(), path))?;
    Ok(Location {
        uri: Url::from_file_path(&path).map_err(|_| anyhow!("path '{path}' is not absolute"))?,
        range: Range {
            start: Position::new(start_line as u32 - 1, start_column as u32 - 1),
            end: Position::new(end_line as u32 - 1, end_column as u32 - 1),
        },
    })
}

struct LspDiagnosticsEmitter {
    /// All diagnostics and which file they are located in.
    diagnostics: Arc<Mutex<Vec<(Url, Diagnostic)>>>,
    /// Handle to the LSP client
    client: Arc<Client>,
}

impl LspDiagnosticsEmitter {
    fn try_emit_diagnostic(
        &mut self,
        diag: &SpadeDiagnostic,
        code: &CodeBundle,
    ) -> color_eyre::Result<()> {
        let (span, file_id) = diag.labels.span;
        let Location { uri, range } = loc_to_location(Loc::new((), span, file_id), code)
            .with_context(|| format!("diagnostic was {diag:?}"))?;
        self.diagnostics.lock().unwrap().push((
            uri,
            Diagnostic {
                range,
                severity: Some(match diag.level {
                    SpadeDiagnosticLevel::Bug => DiagnosticSeverity::ERROR,
                    SpadeDiagnosticLevel::Error => DiagnosticSeverity::ERROR,
                    SpadeDiagnosticLevel::Warning => DiagnosticSeverity::WARNING,
                }),
                code: None,
                code_description: None,
                source: Some("Spade Language Server".to_string()),
                message: diag.labels.message.as_str().to_string(),
                related_information: None, // FIXME subdiagnostics here
                tags: None,
                data: None,
            },
        ));
        Ok(())
    }
}

impl Emitter for LspDiagnosticsEmitter {
    fn emit_diagnostic(&mut self, diag: &SpadeDiagnostic, _buffer: &mut Buffer, code: &CodeBundle) {
        match self.try_emit_diagnostic(diag, code) {
            Ok(()) => (),
            Err(e) => {
                let client = Arc::clone(&self.client);
                Handle::current().spawn(async move {
                    client
                        .log_message(
                            MessageType::ERROR,
                            format!("Error emitting diagnostic: {e:?}"),
                        )
                        .await;
                });
            }
        }
    }
}

#[derive(Debug)]
struct Backend {
    /// Handle to the LSP client.
    client: Arc<Client>,
    /// The root folder that is opened in the client.
    root_dir: Arc<Mutex<Option<Utf8PathBuf>>>,
    symbols: Arc<Mutex<Vec<SymbolInformation>>>,
    /// Cached changes to files.
    ///
    /// We want to compile files that are being changed before they are saved, so this HashMap keeps track
    /// of what the files contains in the WIP state. The paths are canonicalized.
    changed_files: Arc<RwLock<HashMap<Utf8PathBuf, String>>>,
}

macro_rules! try_or_warn {
    ($client:expr, $expr:expr, $prefix:expr $(,)?) => {
        if let Err(e) = $expr {
            $client
                .log_message(MessageType::WARNING, format!("{}{:#}", $prefix, e))
                .await;
        }
    };
}

impl Backend {
    async fn try_compile(&self, file: &Utf8Path, version: Option<i32>) {
        let maybe_root_dir = self.root_dir.lock().unwrap().as_ref().map(Clone::clone);
        // FIXME look upwards for swim.toml?
        let has_swim_toml = maybe_root_dir
            .as_ref()
            .map(|root_dir| root_dir.join("swim.toml").exists())
            .unwrap_or(false);
        match (maybe_root_dir, has_swim_toml) {
            (Some(root_dir), true) => {
                try_or_warn!(
                    self.client,
                    self.try_compile_swim(&root_dir, version).await,
                    "When compiling swim project: ",
                );
            }
            (_, false) => {
                // Try to compile the "current" file.
                try_or_warn!(
                    self.client,
                    self.try_compile_file(file, version).await,
                    format!("When compiling {file}: ")
                )
            }
            (None, true) => unreachable!("Can't have swim.toml without a root_dir to start from"),
        }
    }

    async fn try_compile_file(
        &self,
        file: &Utf8Path,
        version: Option<i32>,
    ) -> color_eyre::Result<()> {
        self.try_compile_files(
            &[SpadeFile {
                namespace: Namespace {
                    namespace: "proj".to_string(),
                    base_namespace: "proj".to_string(),
                },
                path: file.to_path_buf(),
            }],
            version,
        )
        .await
    }

    async fn try_compile_swim(
        &self,
        root_dir: &Utf8Path,
        version: Option<i32>,
    ) -> color_eyre::Result<()> {
        let swim_toml = root_dir.join("swim.toml");
        if !swim_toml.exists() {
            bail!("swim.toml doesn't exist");
        }
        if !swim_toml.is_file() {
            bail!("swim.toml isn't a file");
        }
        self.client
            .log_message(MessageType::LOG, format!("Reading {swim_toml:?}"))
            .await;
        let config = swim::config::Config::read(root_dir, &None)?;

        // Get Spade repository, which clones the compiler repository if needed. This ensures
        // we have the standard library on disk.
        swim::spade::get_spade_repository(root_dir, &config, RestoreAction::Deny)?;

        let mut lock_file = LockFile::open_or_default(lock_file(root_dir));
        let library_dirs = swim::libraries::load_libraries(
            &libs_dir(root_dir),
            &config,
            &mut lock_file,
            RestoreAction::Deny,
        )?;
        let library_files: Vec<_> = library_dirs
            .iter()
            .map(|(name, dir)| swim::spade::spade_files_in_dir(Namespace::new_lib(name), dir))
            .collect::<color_eyre::Result<Vec<_>>>()?
            .into_iter()
            .flatten()
            .collect();
        // NOTE: We deliberately do not filter the lock file at this stage, since we only want
        // user-run commands (like `swim build`) to change it.

        let self_files =
            swim::spade::spade_files_in_dir(Namespace::new_lib("proj"), src_dir(root_dir))?;

        let spade_files: Vec<_> = self_files.into_iter().chain(library_files).collect();

        self.try_compile_files(&spade_files, version).await
    }

    async fn try_compile_files(
        &self,
        files: &[SpadeFile],
        version: Option<i32>,
    ) -> color_eyre::Result<()> {
        let file_names: Vec<String> = files.iter().map(|f| f.path.to_string()).collect();
        self.client
            .log_message(
                MessageType::LOG,
                format!("compiling {}", file_names.join(", ")),
            )
            .await;
        let mut buffer = Buffer::no_color();
        let sources = files
            .iter()
            .map(|SpadeFile { namespace, path }| {
                let file_contents_on_disk = std::fs::read_to_string(path)?;
                let file_contents = self
                    .changed_files
                    .read()
                    .unwrap()
                    .get(path)
                    .map(String::to_string)
                    .unwrap_or(file_contents_on_disk);
                Ok((
                    ModuleNamespace {
                        namespace: spade_path(&namespace.namespace),
                        base_namespace: spade_path(&namespace.base_namespace),
                    },
                    path.to_string(),
                    file_contents,
                ))
            })
            .collect::<color_eyre::Result<Vec<_>>>()?
            .into_iter()
            .chain(stdlib_and_prelude())
            .collect::<Vec<_>>();
        let opts = spade::Opt {
            error_buffer: &mut buffer,
            outfile: None,
            mir_output: None,
            state_dump_file: None,
            item_list_file: None,
            print_type_traceback: false,
            print_parse_traceback: false,
            verilator_wrapper_output: None,
            wl_infer_method: None,
        };
        let diagnostics = Arc::new(Mutex::new(Vec::new()));
        let diag_handler = DiagHandler::new(Box::new(LspDiagnosticsEmitter {
            diagnostics: Arc::clone(&diagnostics),
            client: Arc::clone(&self.client),
        }));
        let compile_result = spade::compile(sources, false, opts, diag_handler);
        let diagnostics = std::mem::take(&mut *diagnostics.lock().unwrap());
        self.client
            .log_message(MessageType::LOG, format!("diagnostics: {diagnostics:?}"))
            .await;
        let mut diagnostics_per_file: HashMap<Url, Vec<Diagnostic>> = files
            .iter()
            .map(|spade_file| {
                (
                    Url::from_file_path(spade_file.path.to_path_buf().canonicalize_utf8().unwrap())
                        .unwrap(),
                    Vec::new(),
                )
            })
            .collect();
        for (file, diagnostic) in diagnostics {
            diagnostics_per_file
                .get_mut(&file)
                .unwrap()
                .push(diagnostic);
        }
        for (uri, diagnostics) in diagnostics_per_file {
            self.client
                .log_message(MessageType::LOG, format!("{uri}: {diagnostics:?}"))
                .await;
            self.client
                .publish_diagnostics(uri, diagnostics, version)
                .await;
        }
        if !buffer.is_empty() {
            self.client
                .log_message(
                    MessageType::LOG,
                    format!(
                        "Got a codespan-style error: {}",
                        String::from_utf8_lossy(buffer.as_slice())
                    ),
                )
                .await;
        }

        match compile_result {
            Ok(Artefacts {
                code, item_list, ..
            })
            | Err(UnfinishedArtefacts {
                code,
                item_list: Some(item_list),
                ..
            }) => {
                self.set_symbols(&item_list, &code).await?;
            }
            Err(_) => (),
        }

        Ok(())
    }

    // Field 'deprecated' in SymbolInformation is deprecated (the irony!)
    // but we still have to specify it.
    #[allow(deprecated)]
    async fn set_symbols(&self, item_list: &ItemList, code: &CodeBundle) -> Result<()> {
        // FIXME: Only pushes entities, functions and pipelines.
        self.client
            .log_message(
                MessageType::LOG,
                format!("Found {} symbols", item_list.executables.len()),
            )
            .await;
        for (i, id) in item_list.executables.keys().enumerate() {
            self.client
                .log_message(MessageType::LOG, format!("{i}: {id:?}"))
                .await;
        }
        *self.symbols.lock().unwrap() = item_list
            .executables
            .iter()
            .filter_map(|(_id, executable)| match executable {
                spade_hir::ExecutableItem::EnumInstance {
                    base_enum: _,
                    variant: _,
                } => None,
                spade_hir::ExecutableItem::StructInstance => None,
                spade_hir::ExecutableItem::Unit(unit) => {
                    Some((unit.name.to_string(), unit.loc(), SymbolKind::FUNCTION))
                }
                spade_hir::ExecutableItem::BuiltinUnit(_, unit_head) => Some((
                    unit_head.name.to_string(),
                    unit_head.loc(),
                    SymbolKind::FUNCTION,
                )),
            })
            .filter_map(|(name, loc, kind)| {
                loc_to_location(loc, code)
                    // FIXME: Report error here (can't currently since it requires an async context).
                    .ok()
                    .map(|location| (name, location, kind))
            })
            .map(|(name, location, kind)| SymbolInformation {
                name,
                location,
                kind,
                tags: None,
                deprecated: None,
                // rust-analyzer uses crates for container_name
                container_name: None,
            })
            .collect();
        Ok(())
    }
}

#[tower_lsp::async_trait]
impl LanguageServer for Backend {
    async fn initialize(&self, params: InitializeParams) -> Result<InitializeResult> {
        self.client
            .log_message(MessageType::LOG, "initialize")
            .await;
        self.client
            .log_message(MessageType::LOG, format!("root_uri: {:?}", params.root_uri))
            .await;
        let root_dir = params
            .root_uri
            .map(|uri| Utf8PathBuf::from(uri.path().to_string()));
        *self.root_dir.lock().unwrap() = root_dir;
        Ok(InitializeResult {
            capabilities: ServerCapabilities {
                text_document_sync: Some(TextDocumentSyncCapability::Kind(
                    TextDocumentSyncKind::FULL,
                )),
                workspace_symbol_provider: Some(OneOf::Left(true)),
                ..ServerCapabilities::default()
            },
            server_info: None,
        })
    }

    async fn initialized(&self, _: InitializedParams) {
        self.client
            .log_message(MessageType::INFO, "Started the Spade Language Server")
            .await;
    }

    async fn did_open(&self, params: DidOpenTextDocumentParams) {
        let path = Utf8PathBuf::from(params.text_document.uri.path().to_string())
            .canonicalize_utf8()
            .unwrap();
        self.client
            .log_message(MessageType::LOG, format!("did_open: {}", path))
            .await;
        self.try_compile(&path, Some(params.text_document.version))
            .await;
    }

    async fn did_change(&self, params: DidChangeTextDocumentParams) {
        let path = Utf8PathBuf::from(params.text_document.uri.path().to_string())
            .canonicalize_utf8()
            .unwrap();
        *self
            .changed_files
            .write()
            .unwrap()
            .entry(path.clone())
            // NOTE: We get only one change containing the entire file since we specified
            // TextDocumentSyncKind::FULL in our ServerCapabilities. Therefore we can
            // overwrite the old String with the new one.
            .or_insert_with(String::new) = params.content_changes[0].text.clone();
        self.client
            .log_message(MessageType::LOG, format!("did_change: {}", path))
            .await;
        self.try_compile(&path, Some(params.text_document.version))
            .await;
    }

    async fn did_save(&self, params: DidSaveTextDocumentParams) {
        let path = Utf8PathBuf::from(params.text_document.uri.path().to_string())
            .canonicalize_utf8()
            .unwrap();
        let _ = self.changed_files.write().unwrap().remove(&path);
    }

    async fn symbol(
        &self,
        params: WorkspaceSymbolParams,
    ) -> Result<Option<Vec<SymbolInformation>>> {
        self.client
            .log_message(
                MessageType::LOG,
                format!("workspace/symbol params: {:?}", params),
            )
            .await;
        let _query = params.query;

        let symbols = self.symbols.lock().unwrap();
        Ok(Some(symbols.clone()))
    }

    async fn shutdown(&self) -> Result<()> {
        Ok(())
    }
}

fn spade_path(s: &str) -> spade_common::name::Path {
    if s.is_empty() {
        return spade_common::name::Path(vec![]);
    }
    let parts = s
        .split("::")
        .map(|ident| Identifier(ident.to_string()).nowhere())
        .collect();
    spade_common::name::Path(parts)
}

#[tokio::main]
async fn main() {
    let stdin = tokio::io::stdin();
    let stdout = tokio::io::stdout();

    let (service, socket) = LspService::new(|client| Backend {
        client: Arc::new(client),
        symbols: Arc::new(Mutex::new(Vec::new())),
        root_dir: Arc::new(Mutex::new(None)),
        changed_files: Arc::new(RwLock::new(HashMap::new())),
    });
    Server::new(stdin, stdout, socket).serve(service).await;
}
